<?php

namespace Database\Factories;

use App\Models\{Student, User};
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Record>
 */
class RecordFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'type'       => fake()->word(),
            'user_id'    => User::factory(),
            'student_id' => Student::factory(),
        ];
    }
}
